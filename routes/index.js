const express = require('express');
const router = express.Router();

const Cart = require('../models/cart');
const Product = require('../models/product');

/* GET home page. */
router.get('/', function (req, res) {
  Product.find(function (err, docs) {
    let productChunks = [];
    let chunkSize = 3;
    for (let i = 0; i < docs.length; i += chunkSize) {
      productChunks.push(docs.slice(i, i + chunkSize));
    }
    res.render('shop/index', {
      title: 'E-Commerce',
      products: productChunks
    });
  });
});

router.get('/add-cart/:id', (req, res) => {
  let productId = req.params.id;
  let cart = new Cart(req.session.cart ? req.session.cart : {});

  Product.findById(productId, function(err, product) {
    if (err) {
      return res.redirect('/');
    }
    cart.add(product, product.id);
    req.session.cart = cart;
    console.log(req.session.cart);
    
    res.redirect('/');
  })
});

router.get('/cart', (req, res) => {
  if (!req.session.cart) {
    return res.render('shop/cart-shop', {product: null});
  }
  let cart = new Cart(req.session.cart);
  res.render('shop/cart-shop', {products: cart.generateArray(), totalPrice: cart.totalPrice})
});

module.exports = router;