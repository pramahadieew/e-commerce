const express = require('express');
const router = express.Router();

let csrf = require('csurf');
let passport = require('passport');

let csrfProtection = csrf();
router.use(csrfProtection);

const allprofile = '/profile';
const allsignup = '/signup';
const allsignin = '/signin';

router.get('/logout', isLoggedIn, (req, res) => {
    req.logout();
    res.redirect('/');
});

router.get(allprofile, isLoggedIn, function (req, res, next) {
    res.render('user/profile');
});

router.use('/', notLoggedIn, function (req, res, next) {
    next();
});

router.get(allsignup, function (req, res, next) {
    let messages = req.flash('error');
    res.render('user/signup', {
        csrfToken: req.csrfToken(),
        messages: messages,
        hasErrors: messages.length > 0
    });
});

router.post(allsignup, passport.authenticate('local.signup', {
    successRedirect: '/user/profile',
    failureRedirect: '/user/signup',
    failureFlash: true
}));

router.get(allsignin, function (req, res, next) {
    let messages = req.flash('error');
    res.render('user/signin', {
        csrfToken: req.csrfToken(),
        messages: messages,
        hasErrors: messages.length > 0
    });
});

router.post(allsignin, passport.authenticate('local.signin', {
    successRedirect: '/user/profile',
    failureRedirect: '/user/signin',
    failureFlash: true
}));

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}

function notLoggedIn(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}